﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeKR
{
    public class ProgressData
    {
        public string percentage { get; set; }
        public string fileSize { get; set; }
        public string pBandWidth { get; set; }
    }
}
