﻿using MetroFramework.Forms;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoutubeKR
{
    public partial class Main : MetroForm
    {
        public Main()
        {
            InitializeComponent();
        }

        private RegistryKey reg;
        private Proc proc = Proc.Instance;

        private string downloadUrl;
        private List<Encoding> encoding;
        private void Main_Load(object sender, EventArgs e)
        {
            reg = Registry.LocalMachine.CreateSubKey("Software").CreateSubKey("YoutubeKR");

            // 레지에서 저장경로 값 가져옴.
            string path = Convert.ToString(reg.GetValue("DownloadPath", ""));
            folderPath.Text = path;
        }

        private void BrowseFolder_Click(object sender, EventArgs e)
        {
            if (fb.ShowDialog() == DialogResult.OK)
            {
                string path = fb.SelectedPath;
                folderPath.Text = path;
                reg.SetValue("DownloadPath", path);
            }
        }

        private void SearchYoutube_Click(object sender, EventArgs e)
        {
            downloadUrl = youtubeLink.Text;
            encoding = proc.Encoding(downloadUrl);
            foreach (Encoding en in encoding)
            {
                List<string> arr = new List<string>();
                arr.Add(en.EXTENSION);
                arr.Add(en.RESOLUTION);
                arr.Add(en.NOTE);
                ListViewItem lvi = new ListViewItem(arr.ToArray());
                formatList.Items.Add(lvi);
            }
        }

        private async void FormatList_MouseDoubleClickAsync(object sender, MouseEventArgs e)
        {
            if (formatList.SelectedIndices.Count > 0)
            {
                int id = formatList.SelectedIndices[0];
                Encoding en = encoding[id];
                string downloadPath = folderPath.Text;
                progress.Value = 0;
                totalByte.Text = "";
                bandWidth.Text = "";
                DialogResult result = MessageBox.Show(en.NOTE + "\n위 포맷으로 다운로드 하시겠습니까?", "다운로드 확인", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    formatList.Enabled = false;
                    youtubeLink.Enabled = false;
                    var objTask = Task<string>.Run(() => DownloadYoutube(en, downloadPath, downloadUrl));
                    await objTask;
                    formatList.Enabled = true;
                    youtubeLink.Enabled = true;
                }
            }
        }

        private void DownloadYoutube(Encoding en, string downloadPath, string downloadUrl)
        {
            string args = string.Format("-o \"{0}\\%(title)s.%(ext)s\" -f {1} {2}", downloadPath, en.FORMAT_CODE, downloadUrl);
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "youtube-dl.exe",
                    Arguments = args,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            proc.Start();
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                if (line.StartsWith("[download]") && line.Contains("% of"))
                {
                    string percentage, pTotalByte, pBandWidth;
                    line = line.Replace("[download]", "").Trim();
                    string[] v = line.Split(new string[] { " of " }, StringSplitOptions.None);
                    percentage = v[0].Trim();
                    if (v[1].Contains("ETA") && !v[1].Contains("Unknown"))
                    {
                        string[] v2 = v[1].Split(new string[] { " at " }, StringSplitOptions.None);
                        pTotalByte = v2[0];
                        string[] v3 = v2[1].Split(new string[] { "ETA" }, StringSplitOptions.None);
                        pBandWidth = v3[0];
                        //ProgressDownload(percentage, totalByte, bandWidth);
                        BeginInvoke((MethodInvoker)delegate
                        {
                            bandWidth.Text = "전송속도 : " + pBandWidth;
                            bandWidth.Update();
                            //Console.WriteLine(pBandWidth);
                            if (!totalByte.Text.Equals(pTotalByte))
                                totalByte.Text = "전체 용량 : " + pTotalByte;
                            totalByte.Update();
                            int pt = (int)double.Parse(percentage.Replace("%", "").Trim());
                            progress.Value = pt;
                            progress.Update();
                        });
                    }
                }
            }
            proc.WaitForExit();
            proc.Close();
        }
    }
}
