﻿namespace YoutubeKR
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.folderPath = new MetroFramework.Controls.MetroTextBox();
            this.browseFolder = new MetroFramework.Controls.MetroButton();
            this.fb = new System.Windows.Forms.FolderBrowserDialog();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.youtubeLink = new MetroFramework.Controls.MetroTextBox();
            this.searchYoutube = new MetroFramework.Controls.MetroButton();
            this.formatList = new System.Windows.Forms.ListView();
            this.Extension = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Resolution = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Notes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.progress = new MetroFramework.Controls.MetroProgressBar();
            this.bandWidth = new MetroFramework.Controls.MetroLabel();
            this.totalByte = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(24, 74);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(65, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "저장경로";
            // 
            // folderPath
            // 
            this.folderPath.Enabled = false;
            this.folderPath.Location = new System.Drawing.Point(95, 73);
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(598, 23);
            this.folderPath.TabIndex = 1;
            // 
            // browseFolder
            // 
            this.browseFolder.Location = new System.Drawing.Point(702, 73);
            this.browseFolder.Name = "browseFolder";
            this.browseFolder.Size = new System.Drawing.Size(75, 23);
            this.browseFolder.TabIndex = 2;
            this.browseFolder.Text = "폴더지정";
            this.browseFolder.Click += new System.EventHandler(this.BrowseFolder_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(24, 122);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(83, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "유튜브 링크";
            // 
            // youtubeLink
            // 
            this.youtubeLink.AllowDrop = true;
            this.youtubeLink.BackColor = System.Drawing.Color.White;
            this.youtubeLink.CustomBackground = true;
            this.youtubeLink.Location = new System.Drawing.Point(113, 122);
            this.youtubeLink.Name = "youtubeLink";
            this.youtubeLink.Size = new System.Drawing.Size(580, 23);
            this.youtubeLink.Style = MetroFramework.MetroColorStyle.Blue;
            this.youtubeLink.TabIndex = 4;
            this.youtubeLink.UseStyleColors = true;
            // 
            // searchYoutube
            // 
            this.searchYoutube.Location = new System.Drawing.Point(702, 122);
            this.searchYoutube.Name = "searchYoutube";
            this.searchYoutube.Size = new System.Drawing.Size(75, 23);
            this.searchYoutube.TabIndex = 5;
            this.searchYoutube.Text = "검색";
            this.searchYoutube.Click += new System.EventHandler(this.SearchYoutube_Click);
            // 
            // formatList
            // 
            this.formatList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Extension,
            this.Resolution,
            this.Notes});
            this.formatList.FullRowSelect = true;
            this.formatList.GridLines = true;
            this.formatList.Location = new System.Drawing.Point(24, 163);
            this.formatList.Name = "formatList";
            this.formatList.Size = new System.Drawing.Size(753, 294);
            this.formatList.TabIndex = 6;
            this.formatList.UseCompatibleStateImageBehavior = false;
            this.formatList.View = System.Windows.Forms.View.Details;
            this.formatList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.FormatList_MouseDoubleClickAsync);
            // 
            // Extension
            // 
            this.Extension.Text = "확장자";
            this.Extension.Width = 71;
            // 
            // Resolution
            // 
            this.Resolution.Text = "해상도";
            this.Resolution.Width = 143;
            // 
            // Notes
            // 
            this.Notes.Text = "영상 정보";
            this.Notes.Width = 530;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(23, 463);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(754, 23);
            this.progress.Step = 1;
            this.progress.Style = MetroFramework.MetroColorStyle.Green;
            this.progress.TabIndex = 7;
            this.progress.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // bandWidth
            // 
            this.bandWidth.AutoSize = true;
            this.bandWidth.Location = new System.Drawing.Point(633, 489);
            this.bandWidth.Name = "bandWidth";
            this.bandWidth.Size = new System.Drawing.Size(0, 0);
            this.bandWidth.TabIndex = 8;
            // 
            // totalByte
            // 
            this.totalByte.AutoSize = true;
            this.totalByte.Location = new System.Drawing.Point(23, 489);
            this.totalByte.Name = "totalByte";
            this.totalByte.Size = new System.Drawing.Size(76, 19);
            this.totalByte.TabIndex = 9;
            this.totalByte.Text = "전체 용량 :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::YoutubeKR.Properties.Resources.youtube;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(201, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 525);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.totalByte);
            this.Controls.Add(this.bandWidth);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.formatList);
            this.Controls.Add(this.searchYoutube);
            this.Controls.Add(this.youtubeLink);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.browseFolder);
            this.Controls.Add(this.folderPath);
            this.Controls.Add(this.metroLabel1);
            this.Font = new System.Drawing.Font("나눔고딕", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Padding = new System.Windows.Forms.Padding(20, 70, 20, 23);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Text = "유튜브 다운로더";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox folderPath;
        private MetroFramework.Controls.MetroButton browseFolder;
        private System.Windows.Forms.FolderBrowserDialog fb;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox youtubeLink;
        private MetroFramework.Controls.MetroButton searchYoutube;
        private System.Windows.Forms.ListView formatList;
        private System.Windows.Forms.ColumnHeader Extension;
        private System.Windows.Forms.ColumnHeader Resolution;
        private System.Windows.Forms.ColumnHeader Notes;
        private MetroFramework.Controls.MetroProgressBar progress;
        private MetroFramework.Controls.MetroLabel bandWidth;
        private MetroFramework.Controls.MetroLabel totalByte;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}