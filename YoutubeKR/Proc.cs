﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace YoutubeKR
{
    public sealed class Proc
    {
        private static volatile Proc instance;
        private static object syncRoot = new Object();

        public Proc() { }

        public static Proc Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Proc();
                    }
                }

                return instance;
            }
        }

        public List<Encoding> Encoding(string url)
        {
            List<Encoding> encoding = new List<Encoding>();
            StringBuilder sb = Youtube("-F " + url);

            string[] delim = { Environment.NewLine, "\n" };
            string[] lines = sb.ToString().Split(delim, StringSplitOptions.None);
            if(lines != null && lines.Length >= 4)
            {
                for (int i = 4; i < lines.Length; i++)
                {
                    string line = lines[i];
                    Regex regex = new Regex("[\\s]+");
                    string[] v = regex.Split(line);
                    if (v != null && v.Length > 5)
                    {
                        StringBuilder notes = new StringBuilder();
                        Encoding e = new Encoding();
                        e.FORMAT_CODE = v[0];
                        e.EXTENSION = v[1];
                        if (v[2].Equals("audio"))
                        {
                            e.RESOLUTION = "오디오 전용";
                            for (int j = 4; j < v.Length; j++)
                                notes.Append(v[j] + " ");
                        }
                        else
                        {
                            e.RESOLUTION = v[2];
                            for (int j = 3; j < v.Length; j++)
                                notes.Append(v[j] + " ");
                        }
                        e.NOTE = notes.ToString();
                        encoding.Add(e);
                    }
                }
            }

            return encoding;
        }

        private StringBuilder Youtube(string args)
        {
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "youtube-dl.exe",
                    Arguments = args,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            proc.Start();
            StringBuilder sb = new StringBuilder();
            while (!proc.StandardOutput.EndOfStream)
            {
                string line = proc.StandardOutput.ReadLine();
                Console.WriteLine(line);
                sb.Append(line).AppendLine();
            }
            proc.WaitForExit();
            proc.Close();

            return sb;
        }
    }
}