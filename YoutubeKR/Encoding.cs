﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YoutubeKR
{
    public class Encoding
    {
        public string FORMAT_CODE { get; set; }
        public string EXTENSION { get; set; }
        public string RESOLUTION { get; set; }
        public string NOTE { get; set; }
    }
}
